@echo off
cls
echo -----------------------------------
echo [START] Build NvsS 2020 Images Pack
echo -----------------------------------

set rootDir=%CD%
set srcDir=%rootDir%\src
set distDir=%rootDir%\dist

echo Build Parameters :
echo Root Dir : %rootDir%
echo Src Dir : %srcDir%
echo Dist Dir : %distDir%

echo Init Dist Packs :
robocopy "%srcDir%\default" "%distDir%\default" /e
robocopy "%srcDir%\default" "%distDir%\old" /e
robocopy "%srcDir%\default" "%distDir%\wil" /e

echo Override with each pack custom files :
robocopy "%srcDir%\old" "%distDir%\old" /e
robocopy "%srcDir%\wil" "%distDir%\wil" /e

echo -----------------------------------
echo [END] Build NvsS 2020 Images Pack
echo -----------------------------------
pause
